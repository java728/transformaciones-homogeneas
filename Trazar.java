/**
 * Compialacion javac Trazar.java
 * Ejecucion java Trazar
 * Modificacion realizada por Barroso Garcia Jhonatan
 * Esta es la clase principal del programa, contiene todas las opciones de dibujo que se solicitaron.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.*;
import java.awt.image.BufferedImage;
import java.awt.image.DirectColorModel;
import java.awt.image.WritableRaster;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public class Trazar extends JFrame {
  private int ancho,alto;    
  private boolean dibujo = false, btrasladar = false;
  private String figuraActual;
  private float x, y, tx, ty, ttx, tty, b, t1, t2;
  private Punto tp, auxp;
  private JPanel panel;
  private PanelPaint pPaint;
  private ArrayList puntos;
  private ListIterator iterador;
  private JFrame frame;
  private JMenuBar opciones;
  private JMenu listOpciones;
  private JMenuItem JBdibujar,JBtrasladar, JBrotar, JBescalar, JBsesgado,
            JBlimpiar,Bcerrar;  
  private JMenuItem guardar;
  private BufferedImage offscreenImage, onscreenImage;
  private Graphics2D g2;
  //Panel para la opcion de dibujar
  private JTextField cx;
  private JTextField cY;
  private JTextField nA;
  private JButton btnAceptarD;
  private JPanel dibujoA;
  private JPanel contenedor;
  //Panel para la rotacion 
  private JPanel dibujoR;
  private JCheckBox opc;
  private JTextField nX;
  private JTextField nY;
  private JTextField angulo;
  private JButton btnAceptarR;
  //Panel para la traslacion 
  private JPanel dibujoT;
  private JTextField trasX;
  private JTextField trasY;
  private JButton btnAceptarT;
  //Panel para el escalamiento 
  private JPanel dibujoE;
  private JTextField esX;
  private JTextField esY;
  private JButton btnAceptarE;
  //Panel para el sesgado
  private JPanel dibujoS;
  private JTextField gS;
  private JButton btnAceptarS;
    
    public Trazar() {
    setTitle("Trazador de figuras geometricas");
    setSize(900,700);
    setLocationRelativeTo(null);
    this.setJMenuBar(getOpciones());
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setResizable(false);
    setVisible(true);
    pPaint      = new PanelPaint(900,700,201,0);
    add(pPaint,BorderLayout.CENTER); 
    contenedor= new JPanel();
     int w=pPaint.getWidth();
     int h=pPaint.getHeight();  
     onscreenImage  =new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
     g2= onscreenImage.createGraphics();
     //Inicialización de los componentes faltantes 
     init();

     //Eventos
     EventoBotones e= new EventoBotones();
    JBdibujar.addActionListener(e);
    JBrotar.addActionListener(e);
    JBtrasladar.addActionListener(e);
    JBsesgado.addActionListener(e);
    JBescalar.addActionListener(e);
    JBlimpiar.addActionListener(e);
    Bcerrar.addActionListener(e);
    //Botones 
    btnAceptarD.addActionListener(e);
    btnAceptarR.addActionListener(e);
    opc.addActionListener(e);
    btnAceptarT.addActionListener(e);
    btnAceptarE.addActionListener(e);
    btnAceptarS.addActionListener(e);
    guardar.addActionListener(new Evento());
   }
   public void init(){
       //Panel para ingresar las opciones de dibujo
       dibujoA= new JPanel();
       JLabel corX= new JLabel("Coordenada X:");
       cx= new JTextField(7);
       JLabel corY= new JLabel("Coordenada Y:");
       cY= new JTextField(7);
       JLabel numeroA= new JLabel("Número de aristas:");
       nA= new JTextField(7);
       btnAceptarD= new JButton("Aceptar");
       btnAceptarD.setForeground(Color.WHITE);
       btnAceptarD.setBackground(new Color(0,185,0));
       btnAceptarD.setFont(new Font("Times New Roman",1,16));
       dibujoA.add(corX);
       dibujoA.add(cx);
       dibujoA.add(corY);
       dibujoA.add(cY);
       dibujoA.add(numeroA);
       dibujoA.add(nA);
       dibujoA.add(btnAceptarD);
       
       //Panel para ingresar las opciones de la rotacion 
       dibujoR= new JPanel();
       JLabel ang= new JLabel("Ángulo de rotación:");
       angulo= new JTextField(7);
       opc= new JCheckBox("Girar sobre su centro");
       opc.setSelected(true);
       JLabel corX2= new JLabel("Coordenada X:");
       nX= new JTextField(7);
       nX.setEditable(false);
       nX.setText(ttx+"");
       JLabel corY2= new JLabel("Coordenada Y:");
       nY= new JTextField(7);
       nY.setEditable(false);
       nY.setText(tty+"");
       btnAceptarR= new JButton("Aceptar");
       btnAceptarR.setForeground(Color.WHITE);
       btnAceptarR.setBackground(new Color(0,185,0));
       btnAceptarR.setFont(new Font("Times New Roman",1,16));
       dibujoR.add(ang);
       dibujoR.add(angulo);
       dibujoR.add(opc);
       dibujoR.add(corX2);
       dibujoR.add(nX);
       dibujoR.add(corY2);
       dibujoR.add(nY);
       dibujoR.add(btnAceptarR);
       
       //Panel para ingresar las opciones de la traslacion
       dibujoT= new JPanel();
       JLabel corX3= new JLabel("Traslación en X:");
       trasX= new JTextField(7);
       JLabel corY3= new JLabel("Traslación en Y:");
       trasY= new JTextField(7);
       btnAceptarT= new JButton("Aceptar");
       btnAceptarT.setForeground(Color.WHITE);
       btnAceptarT.setBackground(new Color(0,185,0));
       btnAceptarT.setFont(new Font("Times New Roman",1,16));
       dibujoT.add(corX3);
       dibujoT.add(trasX);
       dibujoT.add(corY3);
       dibujoT.add(trasY);
       dibujoT.add(btnAceptarT);
       
       //Panel para ingresar las opciones del escalamiento
       dibujoE= new JPanel();
       JLabel corX4= new JLabel("Escala en X:");
       esX= new JTextField(7);
       JLabel corY4= new JLabel("Escala en Y:");
       esY= new JTextField(7);
       btnAceptarE= new JButton("Aceptar");
       btnAceptarE.setForeground(Color.WHITE);
       btnAceptarE.setBackground(new Color(0,185,0));
       btnAceptarE.setFont(new Font("Times New Roman",1,16));
       dibujoE.add(corX4);
       dibujoE.add(esX);
       dibujoE.add(corY4);
       dibujoE.add(esY);
       dibujoE.add(btnAceptarE);
       
       //Panel para ingresar las opciones del sesgado
       dibujoS= new JPanel();
       JLabel gSes= new JLabel("Grado de Sesgado:");
       gS= new JTextField(7);
       btnAceptarS= new JButton("Aceptar");
       btnAceptarS.setForeground(Color.WHITE);
       btnAceptarS.setBackground(new Color(0,185,0));
       btnAceptarS.setFont(new Font("Times New Roman",1,16));
       dibujoS.add(gSes);
       dibujoS.add(gS);
       dibujoS.add(btnAceptarS);
             
    }
   
   private class Evento implements ActionListener{
       public void actionPerformed(ActionEvent ee) {
            FileDialog chooser = new FileDialog(frame, "Use a .png or .jpg extension", FileDialog.SAVE);
                chooser.setVisible(true);
                String filename = chooser.getFile();
                if (filename != null) {
                    save(chooser.getDirectory() + File.separator + chooser.getFile());
                }
          
        }
       
    }
    public void agregarPanel(JPanel panel){
        contenedor.add(panel);
        this.add(contenedor,BorderLayout.NORTH);
        contenedor.updateUI();
   }
   public void quitarPanel(){
        contenedor.removeAll();
        contenedor.updateUI();
   }
   public void limpiarpD(){
        cx.setText("");
        cY.setText("");
        nA.setText("");
   }
   public void limpiarpR(){
        angulo.setText("");
        opc.setSelected(true);
        nX.setText(ttx+"");
        nX.setEditable(false);
        nY.setText(tty+"");
        nY.setEditable(false);
        nA.setText("");
   }
   public void limpiarpT(){
        trasX.setText("");
        trasY.setText("");
   }
   public void limpiarpE(){
        esX.setText("");
        esY.setText("");
   }
   public void limpiarpS(){
        gS.setText("");
   }
   
        
    private class EventoBotones implements ActionListener{
        public void actionPerformed(ActionEvent e){
              
            if(e.getSource()==JBdibujar){
                quitarPanel();
                agregarPanel(dibujoA);             
            }else if(e.getSource()==btnAceptarD){
                 try {
                tp     = new Punto(0,0);
                puntos = trazarPol();
                pPaint.dibujar(puntos);
                quitarPanel();
                limpiarpD();
              }
              catch (Exception ex) {
                JOptionPane.showMessageDialog(null,"Inserta x,y y el numero de aristas");   
                limpiarpD();
                quitarPanel();
              }   
            }else if (e.getSource()==opc){
                if(!opc.isSelected()){
                    nX.setEditable(true);
                    nX.setText("");
                    nY.setEditable(true);
                    nY.setText("");
                }else{
                    nX.setEditable(false);
                    nX.setText(ttx+"");
                    nY.setEditable(false);
                    nY.setText(tty+"");
                }
            }else if(e.getSource()==JBrotar){
                 quitarPanel();
                agregarPanel(dibujoR);
            }else if(e.getSource()==btnAceptarR){
            
                if(dibujo == false)
                puntos = trazarPol();                
            try {
                double ang = Math.toRadians(Float.parseFloat(angulo.getText()));
                if (!opc.isSelected()) {
                    x = Float.parseFloat(nX.getText());
                    y = Float.parseFloat(nY.getText()) * -1;                
                    tp = new Punto(x,y);
                }
                else
                    tp = new Punto(ttx,tty);
                    
                iterador = puntos.listIterator();
                if (btrasladar == true)
                {
                /** generamos los nuevos puntos */
                    while(iterador.hasNext())
                    {   
                        auxp       = (Punto)iterador.next();
                        iterador.set(new Punto(auxp.getX()-tx,auxp.getY()-ty)); 
                    }   btrasladar = false;
                }
                    puntos = Transformar.rotar(puntos,ang);
                    pPaint.dibujar(puntos,tp);
                    quitarPanel();
                    limpiarpR();
            }                
            catch (Exception ex) {
                JOptionPane.showMessageDialog(null,"Ingrese nuevamente los datos ");   
                quitarPanel();
                limpiarpR();
            }    
            }else if(e.getSource()==JBtrasladar){
                 quitarPanel();
                agregarPanel(dibujoT);
            }else if(e.getSource()==btnAceptarT){
                if(dibujo == false)
                puntos = trazarPol();                
             try {
                tx     = Float.parseFloat(trasX.getText());
                ty     = Float.parseFloat(trasY.getText())*-1;
                puntos = Transformar.trasladar(puntos,tx,ty);
                pPaint.dibujar(puntos);
                btrasladar = true;
                ttx    = tx + ttx;
                tty    = ty + tty;
                
                limpiarpT();
                quitarPanel();
              }                
            catch (Exception ex) {
                JOptionPane.showMessageDialog(null,"Ingrese nuevamente los datos");   
                
                limpiarpR();
                quitarPanel();
              }
              
            }else if(e.getSource()==JBsesgado){
                quitarPanel();
                agregarPanel(dibujoS);
            }else if(e.getSource()==btnAceptarS){
                if (dibujo == false)
                puntos = trazarPol();
            try {                    
                b      = Float.parseFloat(gS.getText());
                puntos = Transformar.sesgar(puntos,b);
                pPaint.dibujar(puntos);  
                limpiarpS();
                quitarPanel();
            }                        
            catch (Exception ex) {
                JOptionPane.showMessageDialog(null,"Ingrese nuevamente los datos");   
                limpiarpS();
                quitarPanel();
            }
            }else if(e.getSource()==JBescalar){
                 quitarPanel();
                agregarPanel(dibujoE);
            }else if(e.getSource()==btnAceptarE){
                
             if (dibujo == false)
               puntos = trazarPol();                   
            try {
                t1 = Float.parseFloat(esX.getText());
                t2 = Float.parseFloat(esY.getText())*-1;
                puntos = Transformar.escalar(puntos,t1,t2);
                pPaint.dibujar(puntos);
                limpiarpE();
                quitarPanel();
            }                    
            catch(Exception ex) {
                JOptionPane.showMessageDialog(null,"Ingrese nuevamente los datos ");  
                limpiarpE();
                quitarPanel();
            }
            }else if(e.getSource()==JBlimpiar){
                //if (dibujo == true)
                //puntos = trazarPol();
                repaint();
            }else if(e.getSource()==Bcerrar){
                System.exit(0);
            }

        }
    }
    
    public ArrayList getPuntos(){
        return puntos;
    }
   
   /**
    * Menu de opciones que reemplaza los botones
    */    
    public  JMenuBar getOpciones(){
       opciones= new JMenuBar();
       listOpciones= new JMenu("Herramientas >>>");
    JBdibujar   = new JMenuItem("Dibujar");
    JBtrasladar = new JMenuItem("Trasladar");
    JBrotar     = new JMenuItem("Rotar");
    JBescalar   = new JMenuItem("Escalado");
    JBsesgado   = new JMenuItem("Sesgado");
    JBlimpiar   = new JMenuItem("Actualizar");
    Bcerrar     = new JMenuItem("Cerrar programa");
    guardar= new JMenuItem("Guardar imagen como...");
       opciones.add(listOpciones);
       listOpciones.add(JBdibujar);
       listOpciones.add(JBtrasladar);
       listOpciones.add(JBrotar);
       listOpciones.add(JBescalar );
       listOpciones.add(JBsesgado);
       listOpciones.add(JBlimpiar);
       listOpciones.add(guardar);
       listOpciones.add(Bcerrar);
        return opciones;
    }

   public ArrayList trazarPol() {        
       x = Float.parseFloat(cx.getText());
       y = Float.parseFloat(cY.getText())*-1;
       int nAristas =Integer.parseInt(nA.getText());
       puntos = Transformar.trazarPol(x,y,nAristas);
       dibujo = true;
       return puntos;
   }
   
   /***************************************************************************
    *  Save drawing to a file.
    ***************************************************************************/

    /**
     * Saves the drawing to using the specified filename.
     * The supported image formats are JPEG and PNG;
     * the filename suffix must be {@code .jpg} or {@code .png}.
     *
     * @param  filename the name of the file with one of the required suffixes
     * @throws IllegalArgumentException if {@code filename} is {@code null}
     */
    public  void save(String filename) {
        validateNotNull(filename, "filename");
        int w=pPaint.getWidth();
        int h=pPaint.getHeight(); 
        pPaint.repaint();
        pPaint.dibujar(g2,getPuntos());
        g2.setColor(Color.BLACK);
        g2.setFont(new Font("Times New Roman",Font.BOLD,22));
        g2.drawString("TRANSFORMACIONES HOMOGÉNEAS",-440,-290);
        
        g2.dispose();
        File file = new File(filename);
        String suffix = filename.substring(filename.lastIndexOf('.') + 1);

        // png files
        if ("png".equalsIgnoreCase(suffix)) {
            try {
                ImageIO.write(onscreenImage, suffix, file);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

        // need to change from ARGB to RGB for JPEG
        // reference: http://archives.java.sun.com/cgi-bin/wa?A2=ind0404&L=java2d-interest&D=0&P=2727
        else if ("jpg".equalsIgnoreCase(suffix)) {
            WritableRaster raster = onscreenImage.getRaster();
            WritableRaster newRaster;
            newRaster = raster.createWritableChild(0, 0, w, h, 0, 0, new int[] {0, 1, 2});
            DirectColorModel cm = (DirectColorModel) onscreenImage.getColorModel();
            DirectColorModel newCM = new DirectColorModel(cm.getPixelSize(),
                                                          cm.getRedMask(),
                                                          cm.getGreenMask(),
                                                          cm.getBlueMask());
            BufferedImage rgbBuffer = new BufferedImage(newCM, newRaster, false,  null);
            try {
                ImageIO.write(rgbBuffer, suffix, file);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

        else {
            System.out.println("Invalid image file type: " + suffix);
        }
    }
    // throw an IllegalArgumentException if s is null
    private static void validateNotNull(Object x, String name) {
        if (x == null) throw new IllegalArgumentException(name + " is null");
    }
   
   
   
   
    
   public static void main(String[] figura){
       Trazar frame = new Trazar();
    }
}
