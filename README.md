# Transformaciones homogéneas Graficación 

## Instrucciones
Para iniciar el programa, se debe ejecutar la clase Trazar, esta clase contiene todos los elementos gráficos de la interfaz de usuario, que ayudarán a realizar dibujos de distintos polígonos.
Cuenta con las validaciones apropiadas y las modificaciones solicitadas.

## Autor
Jhonatan Barroso García
